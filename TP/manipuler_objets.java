package personnetp;

import java.util.Scanner;

import personne.Personne;
import personneAccessibiliteR.PersonneAccesR;

public class Exercice1 {

	public static void main(String[] args) {
		Personne unePersonne = new Personne();
		unePersonne.afficheDetails();
		
		Scanner saisie = new Scanner(System.in);
		
		System.out.println("Insérer taille : ");
		unePersonne.taille = saisie.nextDouble();
		
		System.out.println("Insérer genre : ");
		unePersonne.taille = saisie.next().charAt(0);
		
		unePersonne.ditBonjour("FR");
		
		System.out.println(unePersonne.determineFamOuHom());
		if(unePersonne.estAdulte()) {
			System.out.println("C'est un adulte !");
		} else {
			System.out.println("Ce n'est pas un adulte !");
		}
		System.out.println("Son IMC est "+unePersonne.determineIMC());
		System.out.println(unePersonne.significationIMC());
		
		System.out.println("Ajout de mon ami: ");
		
		Personne monAmi = new Personne("Os", "Than", 'H', 250, 3.55, 550);
		
		monAmi.afficheTotale();
		
		System.out.println(monAmi.compareAge(unePersonne));
		
		PersonneAccesR autrePersonne = new PersonneAccesR("Drucker", "Michel", 'H', 75, 1.75, 65);
		
		autrePersonne.afficheTotale();
		
		System.out.println("Insérer taille : ");
		autrePersonne.setTaille(saisie.nextDouble());
		
		System.out.println("Nouvelle taille : "+autrePersonne.getTaille());
		
		//System.out.println("Insérer nom : ");
		//autrePersonne.setNom(saisie.nextDouble()); //Le mutateur n'est pas défini
	}
}
