package sequence2;

public class Proposition {
	private boolean entree = false;
	private boolean plat = false;
	private boolean dessert = false;
	private double prix = 0;
	
	public Proposition(double pr, boolean e, boolean p, boolean d) {
		setEntree(e);
		setPlat(p);
		setDessert(d);
		setPrix(pr);
	}
	
	public String getInfo() {
		String output = "";
		if(getEntree()) {
			output += " une entrée";
		}
		if(getPlat()) {
			output += " un plat";
		}
		if(getDessert()) {
			output += " un dessert";
		}
		output += " au prix de "+getPrix()+" euros.";
		return output;
	}

	public boolean getEntree() {
		return entree;
	}

	public void setEntree(boolean entree) {
		this.entree = entree;
	}

	public boolean getPlat() {
		return plat;
	}

	public void setPlat(boolean plat) {
		this.plat = plat;
	}

	public boolean getDessert() {
		return dessert;
	}

	public void setDessert(boolean dessert) {
		this.dessert = dessert;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

}
